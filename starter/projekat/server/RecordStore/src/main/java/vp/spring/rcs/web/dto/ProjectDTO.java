package vp.spring.rcs.web.dto;

import vp.spring.rcs.model.Category;
import vp.spring.rcs.model.Project;


public class ProjectDTO {
	
	private Long id;
	
	private String name;
	
	
	private String description;
	
	private String startDate;
	
	private String endDate;
	
	private double expectedSum;
	
	
	private Category category;
	

	private SecurityUserDTO userDto;
	
	private String imageUrl;
	
	private double collectedSum;
	
	public ProjectDTO() {
		
	}

	public ProjectDTO(Long id, String name, String description, String startDate, String endDate, double expectedSum,
			Category category, SecurityUserDTO userDto, String imageUrl, double collectedSum) {
		super();
		this.id = id;
		this.name = name;
		this.description = description;
		this.startDate = startDate;
		this.endDate = endDate;
		this.expectedSum = expectedSum;
		this.category = category;
		this.userDto = userDto;
		this.imageUrl = imageUrl;
		this.collectedSum = collectedSum;
	}
	
	public ProjectDTO(Project project) {
		this.id=project.getId();
		this.name=project.getName();
		this.description=project.getDescription();
		this.startDate=project.getStartDate();
		this.endDate=project.getEndDate();
		this.expectedSum=project.getExpectedSum();
		this.category=project.getCategory();
		this.userDto=new SecurityUserDTO(project.getUser());
		this.imageUrl=project.getImageUrl();
		this.collectedSum=project.getCollectedSum();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public double getExpectedSum() {
		return expectedSum;
	}

	public void setExpectedSum(double expectedSum) {
		this.expectedSum = expectedSum;
	}

	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	

	public SecurityUserDTO getUserDto() {
		return userDto;
	}

	public void setUserDto(SecurityUserDTO userDto) {
		this.userDto = userDto;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public double getCollectedSum() {
		return collectedSum;
	}

	public void setCollectedSum(double collectedSum) {
		this.collectedSum = collectedSum;
	}

	@Override
	public String toString() {
		return "ProjectDTO [id=" + id + ", name=" + name + ", description=" + description + ", startDate=" + startDate
				+ ", endDate=" + endDate + ", expectedSum=" + expectedSum + ", category=" + category + ", userDto="
				+ userDto + ", imageUrl=" + imageUrl + ", collectedSum=" + collectedSum + "]";
	}
	
	

}
