package vp.spring.rcs.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import vp.spring.rcs.data.FinancierRepository;
import vp.spring.rcs.model.Financier;

@Component
public class FinancierService {
	
	@Autowired
	FinancierRepository financierRepository;
	
	public List<Financier> findAll(){
		return financierRepository.findAll();
	}
	public Page<Financier> findAll(Pageable page){
		return financierRepository.findAll(page);
	}

	public Financier findOne(Long id) {
		return financierRepository.findOne(id);
	}
	
	public Financier save(Financier financier) {
		return financierRepository.save(financier);
	}
	
	public void delete(Long id) {
		financierRepository.delete(id);;
	}

	public Page<Financier> findByProjectId(Long projectId, Pageable page){
		return financierRepository.findByProjectId(projectId, page);
	}
}
