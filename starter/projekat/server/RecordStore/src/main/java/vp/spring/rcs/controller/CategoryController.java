package vp.spring.rcs.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import vp.spring.rcs.model.Category;

import vp.spring.rcs.service.CategoryService;

import vp.spring.rcs.web.dto.CommonResponseDTO;

@RestController
@RequestMapping(value="api/categories")
public class CategoryController {
	
	@Autowired
	CategoryService categoryService;
	
	
	
	@RequestMapping(method=RequestMethod.GET)
	public ResponseEntity<List<Category>> findAll(){
		List<Category> retVal=categoryService.findAll();
		return new ResponseEntity<>(retVal, HttpStatus.OK);
	}
	
	@RequestMapping(value="/{id}",method=RequestMethod.GET)
	public ResponseEntity<Category> getById(@PathVariable Long id){
		Category retVal=categoryService.findOne(id);
		return new ResponseEntity<>(retVal, HttpStatus.OK);
				
	}
	@RequestMapping(value="/{id}", method=RequestMethod.DELETE)
	public ResponseEntity<CommonResponseDTO> delete(@PathVariable Long id){
		Category cat=categoryService.findOne(id);
		if(cat!=null) {
			categoryService.delete(id);
			return new ResponseEntity<>(new CommonResponseDTO("deleted"), HttpStatus.OK);
		}else {
			return new ResponseEntity<>(new CommonResponseDTO("Required category does not exist"), HttpStatus.NOT_FOUND);
		}
	}
	
	@RequestMapping(method=RequestMethod.POST)
	public ResponseEntity<Category> create(@RequestBody Category category){
		Category cat=categoryService.save(category);
		return new ResponseEntity<>(cat, HttpStatus.CREATED);
	}
	
	@RequestMapping(value="/{id}", method=RequestMethod.PUT)
	public ResponseEntity<Category> update(@PathVariable Long id, @RequestBody Category category){
		Category cat=categoryService.findOne(id);
		if(cat==null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
			cat.setName(category.getName());
			cat.setDescription(category.getDescription());
			cat=categoryService.save(category);
			return new ResponseEntity<>(cat, HttpStatus.OK);
		}
		
	
	
	
	

}
