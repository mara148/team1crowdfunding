package vp.spring.rcs.controller;



import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import vp.spring.rcs.data.UserRepository;
import vp.spring.rcs.model.Financier;
import vp.spring.rcs.model.Project;
import vp.spring.rcs.model.user.SecurityUser;
import vp.spring.rcs.service.FinancierService;
import vp.spring.rcs.service.ProjectService;
import vp.spring.rcs.web.dto.CommonResponseDTO;
import vp.spring.rcs.web.dto.FinancierDTO;
import vp.spring.rcs.web.dto.ProjectDTO;

@RestController

public class FinancierController {
	
	@Autowired
	FinancierService financierService;
	
	@Autowired
	UserRepository userRepository;
	
	@Autowired
	ProjectService projectService;
	
	@RequestMapping(value="api/financiers", method=RequestMethod.GET)
	public ResponseEntity<Page<FinancierDTO>> getAll(Pageable page){
		Page<Financier> financiers=financierService.findAll(page);
		int total=financiers.getTotalPages();
		List<FinancierDTO> ret=financiers.getContent().stream()
				.map(FinancierDTO::new)
				.collect(Collectors.toList());
		Page<FinancierDTO> retVal=new PageImpl<FinancierDTO>(ret, page, total);
		return new ResponseEntity<>(retVal, HttpStatus.OK);
	}
	
	
	
	@RequestMapping(value="api/projects/{projectId}/financiers",method=RequestMethod.GET)
	public ResponseEntity<Page<FinancierDTO>> getByProjectId(@PathVariable Long projectId, Pageable page){
		Page<Financier> financiers=financierService.findByProjectId(projectId, page);
		int total=financiers.getTotalPages();
		List<FinancierDTO> ret=financiers.getContent().stream()
				.map(FinancierDTO::new)
				.collect(Collectors.toList());
		Page<FinancierDTO> retVal=new PageImpl<FinancierDTO>(ret, page, total);
		return new ResponseEntity<>(retVal, HttpStatus.OK);
	}
	
	@RequestMapping(value="api/financiers/{id}", method=RequestMethod.GET)
	public ResponseEntity<FinancierDTO> getById(@PathVariable Long id){
		Financier f=financierService.findOne(id);
		return new ResponseEntity<>(new FinancierDTO(f),HttpStatus.OK);
	}
	
	@RequestMapping(value="api/projects/{id}/financiers", method=RequestMethod.POST, params="sum")
	public ResponseEntity<FinancierDTO> create(@PathVariable Long id, @RequestParam double sum){
		Project project=projectService.findOne(id);
		String username=SecurityContextHolder.getContext().getAuthentication().getName();
		SecurityUser user=userRepository.findByUsername(username);
		
		Financier fin=new Financier();
		fin.setProject(project);
		fin.setUser(user);
		fin.setAmount(sum);
		fin=financierService.save(fin);
		FinancierDTO retVal=new FinancierDTO(fin);
		return new ResponseEntity<>(retVal, HttpStatus.OK);
	}
	
	
	@RequestMapping(value="api/financiers/{id}",method=RequestMethod.DELETE)
	public ResponseEntity<CommonResponseDTO> delete(@PathVariable Long id){
		Financier f=financierService.findOne(id);
		if(f!=null) {
			financierService.delete(id);
			return new ResponseEntity<>(new CommonResponseDTO("deleted"), HttpStatus.OK);
		}else {
			return new ResponseEntity<>(new CommonResponseDTO("Required financier does not exist"), HttpStatus.NOT_FOUND);
		}
	}
	
	@RequestMapping(value="api/financiers/{id}", method=RequestMethod.PUT)
	public ResponseEntity<FinancierDTO> update(@PathVariable Long id, @RequestBody FinancierDTO financier){
		Financier retVal=financierService.findOne(id);
		if(retVal==null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}else {
			retVal.setProject(new Project(financier.getProjectDto().getId()," "," "," "," ", null,null,null," ",null));
			retVal.setAmount(financier.getAmount());
			retVal.setUser(new SecurityUser(financier.getUserDto().getId(), " ", " ", " ", " "));
			retVal=financierService.save(retVal);
			return new ResponseEntity<>(new FinancierDTO(retVal),HttpStatus.OK);
		}
	}
}
