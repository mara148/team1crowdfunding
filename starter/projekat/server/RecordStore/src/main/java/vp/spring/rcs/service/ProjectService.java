package vp.spring.rcs.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import vp.spring.rcs.data.ProjectRepository;
import vp.spring.rcs.model.Project;

@Component
public class ProjectService {
	
	@Autowired
	ProjectRepository projectRepository;
	
	
	public Page<Project> findAll(Pageable page){
		return projectRepository.findAll(page);
	}
	public List<Project> findAll(){
		return projectRepository.findAll();
	}

	public Project findOne(Long id) {
		return projectRepository.findOne(id);
	}
	
	public Project save(Project project) {
		return projectRepository.save(project);
	}
	
	public void delete(Long id) {
		projectRepository.delete(id);;
	}
	
	public Page<Project> findByNameContains(String name, Pageable page){
		return projectRepository.findByNameContains(name, page);
	}
	
	public Page<Project> findByCategoryId(Long categoryId, Pageable page){
		return projectRepository.findByCategoryId(categoryId, page);
	}
	
	public Page<Project> findByUserId(Long userId, Pageable page){
		return projectRepository.findByUserId(userId, page);
	}
	
	
}
