package vp.spring.rcs.web.dto;


import vp.spring.rcs.model.Financier;
import vp.spring.rcs.model.Project;
import vp.spring.rcs.model.user.SecurityUser;

public class FinancierDTO {
	
	private Long id;
	private ProjectDTO projectDto;
	private SecurityUserDTO userDto;
	private double amount;
	
	public FinancierDTO(Long id, ProjectDTO projectDto, SecurityUserDTO userDto, double amount) {
		super();
		this.id = id;
		this.projectDto = projectDto;
		this.userDto = userDto;
		this.amount = amount;
	}

	public FinancierDTO() {
		super();
	}
	
	public FinancierDTO(Financier financier) {
		this.id=financier.getId();
		this.projectDto=new ProjectDTO(financier.getProject());
		this.userDto=new SecurityUserDTO(financier.getUser());
		this.amount=financier.getAmount();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public ProjectDTO getProjectDto() {
		return projectDto;
	}

	public void setProjectDto(ProjectDTO projectDto) {
		this.projectDto = projectDto;
	}

	

	public SecurityUserDTO getUserDto() {
		return userDto;
	}

	public void setUserDto(SecurityUserDTO userDto) {
		this.userDto = userDto;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	@Override
	public String toString() {
		return "FinancierDTO [id=" + id + ", projectDto=" + projectDto + ", userDto=" + userDto + ", amount=" + amount
				+ "]";
	}
	
	

	

}
