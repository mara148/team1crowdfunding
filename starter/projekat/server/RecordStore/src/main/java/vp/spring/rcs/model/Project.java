package vp.spring.rcs.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import vp.spring.rcs.model.user.SecurityUser;
import vp.spring.rcs.web.dto.LoginDTO;

@Entity
public class Project {

	@Id
	@GeneratedValue
	private Long id;

	private String name;

	@Column(length = 5000)
	private String description;

	private String startDate;

	private String endDate;

	private Double collectedSum;

	private Double expectedSum;

	private String imageUrl;

	@ManyToOne(fetch = FetchType.EAGER)
	private Category category;

	@ManyToOne(fetch = FetchType.EAGER)
	private SecurityUser user;
	
	
	
	
	
	

	public Project() {
		super();
	}
	
	
	
	public Project(Long id, String name, String description, String startDate, String endDate, Double expectedSum,
			Category category, SecurityUser user, String imageUrl, Double collectedSum) {
		super();
		this.id = id;
		this.name = name;
		this.description = description;
		this.startDate = startDate;
		this.endDate = endDate;
		this.expectedSum = expectedSum;
		this.category = category;
		this.user = user;
		this.imageUrl = imageUrl;
		this.collectedSum = collectedSum;
	}




	public String getImageUrl() {
		return imageUrl;
	}
	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}
	
	
	
	
	public Category getCategory() {
		return category;

	}

	public double getCollectedSum() {
		return collectedSum;
	}

	public String getDescription() {
		return description;
	}

	public String getEndDate() {
		return endDate;
	}

	public Double getExpectedSum() {
		return expectedSum;
	}

	public Long getId() {
		return id;
	}

	public String getImageURl() {
		return imageUrl;
	}

	public String getName() {
		return name;
	}

	public String getStartDate() {
		return startDate;
	}

	public SecurityUser getUser() {
		return user;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	public void setCollectedSum(Double collectedSum) {
		this.collectedSum = collectedSum;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public void setExpectedSum(Double expectedSum) {
		this.expectedSum = expectedSum;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setImageURl(String imageURl) {
		this.imageUrl = imageURl;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public void setUser(SecurityUser user) {
		this.user = user;
	}
	@Override
	public String toString() {
		return "Project [id=" + id + ", name=" + name + ", description=" + description + ", startDate=" + startDate
				+ ", endDate=" + endDate + ", expectedSum=" + expectedSum + ", category=" + category + ", user=" + user
				+ ", collectedSum=" + collectedSum + "]";
	}
	
	

}
