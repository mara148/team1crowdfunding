package vp.spring.rcs.controller;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import vp.spring.rcs.data.UserRepository;
import vp.spring.rcs.model.Project;
import vp.spring.rcs.model.user.SecurityUser;
import vp.spring.rcs.service.ProjectService;
import vp.spring.rcs.web.dto.CommonResponseDTO;
import vp.spring.rcs.web.dto.ProjectDTO;

@RestController

public class ProjectController {
	
	@Autowired
	ProjectService projectService;
	
	@Autowired
	UserRepository userRepository;
	
	
	@RequestMapping(value="api/projects",method=RequestMethod.GET)
	public ResponseEntity<Page<ProjectDTO>> getAll(Pageable page){
		Page<Project> projects=projectService.findAll(page);
		int x=projects.getTotalPages();
		List<ProjectDTO> ret=projects.getContent().stream()
		.map(ProjectDTO::new)
		.collect(Collectors.toList());
		Page<ProjectDTO> retVal=new PageImpl<ProjectDTO>(ret, page, x);
		return new ResponseEntity<>(retVal, HttpStatus.OK);
	
	}
	
	
	
	@RequestMapping(value="api/projects/{id}",method=RequestMethod.GET)
	public ResponseEntity<ProjectDTO> getById(@PathVariable Long id){
		Project retVal=projectService.findOne(id);
		return new ResponseEntity<>(new ProjectDTO(retVal), HttpStatus.OK);
				
	}
	
	@RequestMapping(value="api/projects/{id}", method=RequestMethod.DELETE)
	public ResponseEntity<CommonResponseDTO> delete(@PathVariable Long id){
		Project pro=projectService.findOne(id);
		if(pro!=null) {
			projectService.delete(id);
			return new ResponseEntity<>(new CommonResponseDTO("deleted"), HttpStatus.OK);
		}else {
			return new ResponseEntity<>(new CommonResponseDTO("Required category does not exist"), HttpStatus.NOT_FOUND);
		}
	}
	
	@RequestMapping(value="api/projects",method=RequestMethod.POST)
	public ResponseEntity<ProjectDTO> create(@RequestBody ProjectDTO project){
		
		String username = SecurityContextHolder.getContext().getAuthentication().getName();
		SecurityUser user=userRepository.findByUsername(username);
		Project pro=new Project();
		
		pro.setName(project.getName());
		pro.setDescription(project.getDescription());
		pro.setStartDate(project.getStartDate());
		pro.setEndDate(project.getEndDate());
		pro.setExpectedSum(project.getExpectedSum());
		pro.setCategory(project.getCategory());
		pro.setImageUrl(project.getImageUrl());
		pro.setUser(user);
		pro.setCollectedSum(project.getCollectedSum());
		 pro=projectService.save(pro);
		return new ResponseEntity<>(new ProjectDTO(pro), HttpStatus.CREATED);
	}
	
	@RequestMapping(value="api/projects/{id}", method=RequestMethod.PUT)
	public ResponseEntity<ProjectDTO> update(@PathVariable Long id, @RequestBody ProjectDTO project){
		Project pro=projectService.findOne(id);
		if(pro==null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
			pro.setCategory(project.getCategory());
			pro.setDescription(project.getDescription());
			pro.setEndDate(project.getEndDate());
			pro.setExpectedSum(project.getExpectedSum());
			pro.setName(project.getName());
			pro.setStartDate(project.getStartDate());
			pro.setCollectedSum(project.getCollectedSum());
			pro.setImageUrl(project.getImageUrl());
			pro.setUser(new SecurityUser(project.getUserDto().getId()," ", " ", " ", " "));
			pro=projectService.save(pro);
			return new ResponseEntity<>(new ProjectDTO(pro), HttpStatus.OK);
		}
	
	@RequestMapping(value="api/projects",method=RequestMethod.GET, params="name")
	public ResponseEntity<Object> getByName(@RequestParam String name, Pageable page){
		Page<Project> projects=projectService.findByNameContains(name, page);
		int x=projects.getTotalPages();
		List<ProjectDTO> ret=projects.getContent().stream()
		.map(ProjectDTO::new)
		.collect(Collectors.toList());
		Page<ProjectDTO> retVal=new PageImpl<ProjectDTO>(ret, page, x);
		return new ResponseEntity<Object>(retVal, HttpStatus.OK);
	}
	

	
	@RequestMapping(value="api/users/{userId}/projects", method=RequestMethod.GET)
	public ResponseEntity<Page<ProjectDTO>> getById(@PathVariable Long userId, Pageable page) {
		Page<Project> projects=projectService.findByUserId(userId, page);
		int x=projects.getTotalPages();
		List<ProjectDTO> ret=projects.getContent().stream()
				.map(ProjectDTO::new)
				.collect(Collectors.toList());
		Page<ProjectDTO> retVal=new PageImpl<ProjectDTO>(ret, page, x);
			return new ResponseEntity<>(retVal, HttpStatus.OK);
		}
	
	@RequestMapping(value="api/categories/{categoryId}/projects", method=RequestMethod.GET)
	public ResponseEntity<Page<ProjectDTO>> getByCategory(@PathVariable Long categoryId, Pageable page){
		Page<Project> projects=projectService.findByCategoryId(categoryId, page);
		int x=projects.getTotalPages();
		List<ProjectDTO> ret=projects.getContent().stream()
				.map(ProjectDTO::new)
				.collect(Collectors.toList());
		Page<ProjectDTO> retVal=new PageImpl<ProjectDTO>(ret, page, x);
		return new ResponseEntity<>(retVal, HttpStatus.OK);
	}
	
	@RequestMapping(value="api/projects/{id}", method=RequestMethod.PUT, params="sum")
	public ResponseEntity<ProjectDTO> getCollectedSum(@PathVariable Long id,  @RequestParam double sum){
		Project p=projectService.findOne(id);
		double newSum=p.getCollectedSum()+sum;
		p.setCollectedSum(newSum);
		p=projectService.save(p);
		return new ResponseEntity<>(new ProjectDTO(p), HttpStatus.OK);
		
	}
	



		
}
