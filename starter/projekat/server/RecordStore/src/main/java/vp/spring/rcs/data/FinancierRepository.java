package vp.spring.rcs.data;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import vp.spring.rcs.model.Financier;

@Repository
public interface FinancierRepository extends JpaRepository<Financier, Long>{
	
	public Page<Financier> findAll(Pageable page);
	
	public Page<Financier> findByProjectId(Long projectId, Pageable page);

}
