package vp.spring.rcs.data;




import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import vp.spring.rcs.model.Project;

@Repository
public interface ProjectRepository extends JpaRepository<Project, Long>{

	public Page<Project> findAll(Pageable page);
	
	public Page<Project> findByNameContains(String name, Pageable page);
	
	public Page<Project> findByCategoryId(Long categoryId, Pageable page);
	
	public Page<Project> findByUserId(Long userId, Pageable page);
	
	
	

}
