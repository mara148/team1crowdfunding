package vp.spring.rcs.model;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import vp.spring.rcs.model.user.SecurityUser;

@Entity
public class Financier {
	
	@Id
	@GeneratedValue
	private Long id;
	
	@ManyToOne(fetch =FetchType.EAGER)
	private Project project;
	
	@ManyToOne(fetch =FetchType.EAGER)
	private SecurityUser user;
	
	private double amount;

	public Financier() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Project getProject() {
		return project;
	}

	public void setProject(Project project) {
		this.project = project;
	}

	public SecurityUser getUser() {
		return user;
	}

	public void setUser(SecurityUser user) {
		this.user = user;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

}
