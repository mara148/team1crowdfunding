insert into security_user (username, password, first_name, last_name) values 
	('admin', '$2a$04$4pqDFh9SxLAg/uSH59JCB.LwIS6QoAjM9qcE7H9e2drFuWhvTnDFi', 'Admin', 'Admin');
-- password is abcdef (bcrypt encoded)
insert into security_user (username, password, first_name, last_name) values 
	('petar', '$2a$04$Yr3QD6lbcemnrRNLbUMLBez2oEK15pdacIgfkvymQ9oMhqsEE56EK', 'Petar', 'Petrovic');
insert into security_user (username, password, first_name, last_name) values 
	('milan', '$2a$04$Yr3QD6lbcemnrRNLbUMLBez2oEK15pdacIgfkvymQ9oMhqsEE56EK', 'Milan', 'Milankovic');
insert into security_user (username, password, first_name, last_name) values 
	('zika', '$2a$04$Yr3QD6lbcemnrRNLbUMLBez2oEK15pdacIgfkvymQ9oMhqsEE56EK', 'Zika', 'Zikic');
    insert into security_user (username, password, first_name, last_name) values 
	('Luka', '$2a$04$Yr3QD6lbcemnrRNLbUMLBez2oEK15pdacIgfkvymQ9oMhqsEE56EK', 'Luka', 'Lukic');
insert into security_user (username, password, first_name, last_name) values 
	('Tamara', '$2a$04$Yr3QD6lbcemnrRNLbUMLBez2oEK15pdacIgfkvymQ9oMhqsEE56EK', 'Tamara', 'Peric');
-- insert authorities
insert into security_authority (name) values ('ROLE_ADMIN'); -- super user
insert into security_authority (name) values ('ROLE_USER'); -- normal user

-- insert mappings between users and authorities
insert into security_user_authority (user_id, authority_id) values (1, 1); -- admin has ROLE_ADMIN
insert into security_user_authority (user_id, authority_id) values (1, 2); -- admin has ROLE_USER too
insert into security_user_authority (user_id, authority_id) values (2, 2); -- petar has ROLE_USER
insert into security_user_authority (user_id, authority_id) values (3, 2); 
insert into security_user_authority (user_id, authority_id) values (4, 2); 
insert into security_user_authority (user_id, authority_id) values (5, 2); 
insert into security_user_authority (user_id, authority_id) values (6, 2); 

insert into category(description, name) values ('opis', 'design');
insert into category(description, name) values ('opis', 'tech');
insert into category(description, name) values ('opis', 'food');
insert into category(description, name) values ('opis', 'education');
insert into category(description, name) values ('opis', 'arts');
insert into category(description, name) values ('opis', 'music');

insert into project (collected_sum,description, end_date, expected_sum, image_url, name, start_date, category_id, user_id) values ( 25000,'opis', '01.01.2019', 750000, ' ' ,'project1', '01.09.2018',1,2);
insert into project (collected_sum,description, end_date, expected_sum, image_url, name, start_date, category_id, user_id) values ( 45000,'opis', '01.01.2019', 750000, ' ' ,'project2', '01.09.2018',1,2);
insert into project (collected_sum,description, end_date, expected_sum, image_url, name, start_date, category_id, user_id) values ( 45000,'opis', '01.01.2019', 750000, ' ' ,'project3', '01.09.2018',2,2);
insert into project (collected_sum,description, end_date, expected_sum, image_url, name, start_date, category_id, user_id) values ( 45000,'opis', '01.01.2019', 750000, ' ' ,'project4', '01.09.2018',3,1);
insert into project (collected_sum,description, end_date, expected_sum, image_url, name, start_date, category_id, user_id) values ( 45000,'opis', '01.01.2019', 750000, ' ' ,'project5', '01.09.2018',4,3);


INSERT INTO `db_team1`.`category` (`id`, `description`, `name`) VALUES ('1', 'opis kategorije 1', 'naziv kategorije 1');
INSERT INTO `db_team1`.`category` (`description`, `name`) VALUES ('opis kategorije 2', 'naziv kategorije 2');
INSERT INTO `db_team1`.`category` (`description`, `name`) VALUES ('opis kategorije 3', 'naziv kategorije 3');

INSERT INTO `db_team1`.`project` (`collected_sum`, `description`, `end_date`, `expected_sum`, `image_url`, `name`, `start_date`, `category_id`, `user_id`) VALUES ('0', 'opis projekta  1', '31/12/2018', '10000', 'https://unsplash.com/photos/JCRF5WSUCE4', 'naziv projekta 1', '12/12/2018', '1', '1');
INSERT INTO `db_team1`.`project` (`collected_sum`, `description`, `end_date`, `expected_sum`, `image_url`, `name`, `start_date`, `category_id`, `user_id`) VALUES ('0', 'opis projekta 2', '21/11/2019', '20000', 'https://images.unsplash.com/photo-1535953928289-6bac80e96f3c?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=634&q=80', 'projekta 2', '10/11/2018', '2', '1');

INSERT INTO `db_team1`.`financier` (`amount`, `project_id`, `user_id`) VALUES ('10', '1', '1');





