import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpClient, HttpParams } from '@angular/common/http';
import { AuthenticationService } from 'app/security/authentication.service';
import { HttpHeaders } from '@angular/common/http';
import { ProjectInterface } from 'app/common.models';

@Component({
  selector: 'app-project-details',
  templateUrl: './project-details.component.html',
  styleUrls: ['./project-details.component.css']
})
export class ProjectDetailsComponent implements OnInit {

  isDataAvailable:boolean=false;
  project: ProjectInterface={
    name:'',
    description:'',
    category:{},
    startDate: '',
    endDate: '',
    expectedSum: 0,  
    imageUrl: '',   
    collectedSum: 0  
  }
  financier:any;

  

  donatedAmount: number; 

  public donationClicked:boolean;


  constructor(private route: ActivatedRoute, private http: HttpClient,
    private authenticationService: AuthenticationService) {
      
    } 

  ngOnInit() {
   this.loadData();
   }
  

  loadData(){
    this.route.params.subscribe(params => {
      const id = params['id'];
      this.http.get(`api/projects/${id}`).subscribe(response => {
        this.project = response as ProjectInterface;
        console.log(response);
        this.isDataAvailable=true;
      });
    });
  }

  hasRole(role: string): boolean {
    return this.authenticationService.hasRole(role);
  }

   showDonationForm() { 
    if(this.authenticationService.isLoggedIn()){ 
    this.donationClicked = true;
    }else{
      alert('Please sign in or log in');
    }
  }  

  verifyDonation(){
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    const params = new HttpParams()
    .set('sum', this.donatedAmount.toString());
    this.http.put(`api/projects/${this.project.id}`, JSON.stringify(this.project),{ params })
    .subscribe((data) => {this.loadData();
      this.createFinancier(params);
    });
  }

  createFinancier(params){
    this.http.post(`api/projects/${this.project.id}/financiers`, JSON.stringify(this.project), {params})
    .subscribe(res=> this.financier=res as any);
    
  }
  

}
