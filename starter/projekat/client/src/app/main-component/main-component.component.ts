import { Component, OnInit } from '@angular/core';
import { Response, RequestOptions,
         Headers } from '@angular/http';

import * as _ from 'lodash';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders,HttpParams} from '@angular/common/http';
import { AuthenticationService } from 'app/security/authentication.service';
import { ProjectInterface } from 'app/common.models';


@Component({
  selector: 'app-main-component',
  templateUrl: './main-component.component.html',
  styleUrls: ['./main-component.component.css']
})
export class MainComponentComponent implements OnInit {

projects: ProjectInterface[];

categories: any[];

project: ProjectInterface={
  name: ' ',
  category:{},
  description: ' ',
  startDate: ' ',
  endDate: ' ',
  expectedSum: 0,  
  imageUrl: ' ',   //treba dodati u model
  collectedSum: 0 //treba dodati u model

}

cathegory: any={
  name: '',
  description: ''
}

currentPage = 0;

numberOfPages: number;

ngOnInit(): void {
  this.loadProjects();
}

constructor(private http: HttpClient,private authenticationService: AuthenticationService) {
}

loadProjects() {
  const params = new HttpParams()
    .set('page', this.currentPage.toString())
    .set('size', '1');
  this.http.get('api/projects', { params }).subscribe( data => {     //OVDE TREBA UBACITI PRAVI URL KAD SE NAPRAVI KONTROLER
    this.projects = data['content'] as ProjectInterface[];
    this.numberOfPages = data['totalPages'];
    console.log(data);
    this.reset();
  });
}


hasRole(role: string): boolean {
  return this.authenticationService.hasRole(role);
}

reset() {
  this.project = {
    name: '',
    category:{},
    description: '',
    startDate: '',
    endDate: '',
    imageUrl:'',
    expectedSum: 0,
    collectedSum:0
  };
}

changePage(x: number) {
  if (this.currentPage + x >= 0 && this.currentPage + x < this.numberOfPages) {
    this.currentPage += x;
    this.loadProjects();
  }
}
}
