import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AuthenticationService } from 'app/security/authentication.service';
import { ProjectInterface, CategoryInterface } from 'app/common.models';
import{Router} from '@angular/router';

@Component({
  selector: 'app-add-project',
  templateUrl: './add-project.component.html',
  styleUrls: ['./add-project.component.css']
})
export class AddProjectComponent implements OnInit {

  project: ProjectInterface={
    name: '',
    description: '',
    category:{},
    startDate: '',
    endDate: '',
    expectedSum: 0,  
    imageUrl: '',   
    collectedSum: 0,
    user:{}

  }

  public categories: CategoryInterface[];


  constructor(private http: HttpClient, private router:Router,
     private authenticationService: AuthenticationService) { }
  

  ngOnInit() {
    this.loadCathegories();
  }
   
  loadCathegories(){
    this.http.get('api/categories').subscribe( data => {
      this.categories = data as CategoryInterface[];
    });
  }

  addProject(){
      const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
      this.http.post('api/projects', JSON.stringify(this.project), {headers})
      .subscribe(data=>{this.reset();
        this.router.navigate(['/main']);
      });
      this.loadCathegories();
  }
  
  hasRole(role: string): boolean {
    return this.authenticationService.hasRole(role);
  }

  reset(){
    this.project={
      name: '',
    description: '',
    category:{},
    startDate: '',
    endDate: '',
    expectedSum: 0,  
    imageUrl: '',   
    collectedSum: 0,
    user:{}

    }
  }

}
