export interface CategoryInterface{
    id?:number;
    name?:string;
    description?:string;
}

export interface UserInterface{
    id?:number;
    username?:string;
    password?:string;
    firstName?:string;
    lastName?:string;
}

export interface ProjectInterface{
    id?:number;
    category:CategoryInterface;
    user?: UserInterface;
    name:string;
    description:string;
    startDate:string;
    endDate:string;
    expectedSum:number;  
    imageUrl:string;
    collectedSum:number;

}